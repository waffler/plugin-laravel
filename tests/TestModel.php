<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TestModel.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests
 * @property string $foo
 */
class TestModel extends Model
{
    protected $guarded = [];
}