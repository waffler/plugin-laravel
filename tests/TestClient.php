<?php

namespace Tests;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use Waffler\Definitions\Attributes\Get;
use Waffler\Definitions\Attributes\ReturnsMappedList;
use Waffler\LaravelPlugin\Attributes\InjectConfig;

#[InjectConfig([
    'base_uri' => 'services.test_case_client.base_uri'
])]
interface TestClient
{
    #[Get('/')]
    public function testModel(): TestModel;

    #[Get('/')]
    public function testCollection(): Collection;

    #[Get('/')]
    public function testStringable(): Stringable;

    #[Get('/')]
    #[ReturnsMappedList(AutoMappedInterfaceForTesting::class)]
    public function autoMappedCollectionTest(): Collection;
}