<?php

namespace Tests;

use Illuminate\Support\Facades\Config;
use Orchestra\Testbench\TestCase;
use Waffler\LaravelPlugin\ServiceProvider;

/**
 * Class ClientRegistrationTest.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests
 * @covers
 */
class ClientRegistrationTest extends TestCase
{
    public function test_ensure_the_configuration_is_merged()
    {
        $this->app = \Mockery::mock($this->app);

        $this->app->shouldReceive('mergeConfigFrom')
            ->with(\Mockery::type('string'), 'services');

        $this->app->register(ServiceProvider::class);
    }

    public function test_ensure_the_client_is_registered_in_service_container_and_the_configuration_is_injected()
    {
        Config::partialMock();
        Config::push('services.waffler.clients', TestClient::class);
        Config::set('services.test_case_client', [
            'base_uri' => 'test_case_base_uri'
        ]);
        $this->app->register(ServiceProvider::class);
        $instance = $this->app->make(TestClient::class);
        $reflection = new \ReflectionClass($instance);
        self::assertTrue($this->app->bound(TestClient::class));
        self::assertTrue($reflection->isAnonymous());
        self::assertTrue($reflection->implementsInterface(TestClient::class));
        Config::shouldHaveReceived('get')
            ->with('services.test_case_client.base_uri', 'services.test_case_client.base_uri')
            ->once();
    }
}
