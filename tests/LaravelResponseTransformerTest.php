<?php

namespace Tests;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use Orchestra\Testbench\TestCase;
use Waffler\LaravelPlugin\ResponseTransformers\LaravelResponseTransformer;
use Waffler\Rest\Client;

/**
 * Class LaravelResponseTransformerTest.
 *
 * @covers
 */
class LaravelResponseTransformerTest extends TestCase
{
    private MockHandler $mockHandler;
    private TestClient $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->mockHandler = new MockHandler();
        Client::addResponseTransformer(new LaravelResponseTransformer());
        $this->client = Client::implements(TestClient::class, [
            'handler' => $this->mockHandler
        ]);
    }

    public function test_must_return_model_instance()
    {
        $this->mockHandler->append(
            fn() => new Response(200, body: json_encode(['foo' => 'bar']))
        );

        $response = $this->client->testModel();

        self::assertTrue($response instanceof TestModel);
        self::assertTrue($response->foo === 'bar');
    }

    public function test_must_return_laravel_stringable_instance()
    {
        $this->mockHandler->append(
            fn() => new Response(200, body: json_encode(['foo' => 'bar']))
        );

        $response = $this->client->testStringable();

        self::assertTrue($response instanceof Stringable);
        self::assertTrue($response->__toString() === '{"foo":"bar"}');
    }

    public function test_must_return_laravel_collection_instance()
    {
        $this->mockHandler->append(
            fn() => new Response(200, body: json_encode([['foo' => 'bar']]))
        );

        $response = $this->client->testCollection();

        self::assertTrue($response instanceof Collection);
        self::assertTrue($response->first()['foo'] === 'bar');
    }

    public function test_must_return_laravel_collection_for_auto_mapped_list_methods()
    {
        $this->mockHandler->append(fn() => new Response(body: json_encode([
            ['foo' => 'bar']
        ])));

        $this->client->autoMappedCollectionTest()
            ->each(fn(AutoMappedInterfaceForTesting $instance) => self::assertTrue(
                $instance->getFoo() === 'bar'
            ));
    }
}