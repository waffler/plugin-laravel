<?php

namespace Tests;

use Waffler\Definitions\Attributes\AutoMapped;
use Waffler\Definitions\Attributes\MapTo;

/**
 * Interface AutoMappedInterfaceForTesting.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests
 */
#[AutoMapped]
interface AutoMappedInterfaceForTesting
{
    #[MapTo('foo')]
    public function getFoo(): string;
}