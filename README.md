# plugin-laravel

Laravel integration for waffler/rest

## Requirements

- [x] Laravel ^8.x
- [x] PHP ^8.x

## Installation

```shell
$ composer require waffler/laravel-plugin
```

## Usage

#### Binding the client into the service container:
Like in waffler/rest you'll need to create a new client interface,
after that, put the fully qualified class name into `config/services.php`
in the following config section:

```php
return [
    // ...

    'waffler' => [
        'clients' => [
            // Put here
            \App\Http\Clients\ExampleClientInterface::class,
        ]   
    ]
   
    // ...
]
```

#### Retrieving from service container:

Like any bound service, you can ask to laravel to give you the service
you created.

```php
use App\Http\Clients\MyClientInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MyController extends Controller 
{
    public function index(MyClientInterface $client)
    {
        // Retrieve as dependency injection
        ... 
    }
    
    public function store(Request $request)
    {
        // Retrieve from the service container.

        /** @var MyClientInterface $client */
        $client = app(MyClientInterface::class);
        ...
    }
}
```

## New features

### New Attribute:
You will probably ask how to give some guzzle http configs to your client like
`base_uri`, `defaults`, etc.
Well, you can use the new attribute created just for this package: `#[InjectConfig]`

#### Usage:
The attribute can receive two arguments, the first in an array with the configs you want to give,
like `base_uri`, etc. The second tells to laravel if the Client must be bound as singleton service,
 the default is `false`.

The first argument, the array of guzzle http configs, has two behaviours:
- You can give any literal value.
- You can give any value from the cached laravel config files.

#### Example:
```php
<?php

namespace App\Http\Clients;

use \Waffler\LaravelPlugin\Attributes\InjectConfig;

#[InjectConfig([
    // passing config as literal value:
    'defaults' => [
        'headers' => [...]
    ],
    
    // passing config from laravel cached config files
    'base_uri' => 'services.foo.base_uri', // yes, just like this
])]
interface FooClient
{}
```

### New Return Types:
We added support for three new response types 
that are common in laravel daily programming routine,
and you would probably miss then.

- [x] `\Illuminate\Support\Collection`
- [x] `Illuminate\Database\Eloquent\Model`
- [x] `Illuminate\Support\Stringable`

*The Model response type are any subtype of Model, this means you can put your
`\App\Models\MyModel` as response type and the client will give you what you want.*

#### Example:

```php
// File: TodosClient.php

namespace App\Http\Clients;

use Illuminate\Support\Collection;
use Waffler\Definitions\Attributes\Get;
use Waffler\LaravelPlugin\Attributes\InjectConfig;

#[InjectConfig([
    'base_uri' => 'services.todos.base_uri'
])]
interface TodosClient
{
    #[Get('/todos')]
    public function getAll(): Collection; // We just declare here, and it's ready to use.
}
```

And then the usage:

```php
// File: TodosController.php

public function index(TodosClient $client)
{
    // Just use like you'd expect.

    $todos = $client->getAll()
        ->map(...)
        ->each(...);

    // ...
}
```