<?php

namespace Waffler\LaravelPlugin;


use Illuminate\Support\Facades\Config as LaravelConfig;
use Waffler\LaravelPlugin\Attributes\InjectConfig;
use Waffler\LaravelPlugin\ResponseTransformers\LaravelResponseTransformer;
use Waffler\Rest\Client;

/**
 * Class ServiceProvider.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\LaravelPlugin
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        Client::addResponseTransformer(new LaravelResponseTransformer());
    }

    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(__DIR__.'/config.php', 'services');

        $this->registerServices();
    }

    /**
     * Register all clients into service container.
     *
     * @return void
     */
    private function registerServices(): void
    {
        foreach (LaravelConfig::get('services.waffler.clients') as $serviceInterface) {
            $options = [];
            $shared = false;

            try {
                /**
                 * @var array<\ReflectionAttribute<InjectConfig>>
                 */
                $attributes = (new \ReflectionClass($serviceInterface))
                    ->getAttributes(InjectConfig::class);

                if (count($attributes) > 0) {
                    /** @var InjectConfig $injectConfig */
                    $injectConfig = $attributes[0]->newInstance();

                    $options = $injectConfig->config;
                    $shared = $injectConfig->singleton;

                    foreach ($options as $option => $optionValue) {
                        $options[$option] = LaravelConfig::get($optionValue, $optionValue);
                    }
                }
            } catch (\ReflectionException) {}

            $this->app->bind($serviceInterface, fn() => Client::implements(
                $serviceInterface,
                $options
            ), $shared);
        }
    }
}
