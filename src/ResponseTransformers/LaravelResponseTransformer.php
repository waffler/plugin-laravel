<?php

namespace Waffler\LaravelPlugin\ResponseTransformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use Psr\Http\Message\ResponseInterface;
use Waffler\Definitions\MethodInterface;
use Waffler\Definitions\ResponseTransformer;
use Waffler\Rest\Traits\ResponseDecoder;

/**
 * Class LaravelResponseTransformer.
 *
 * Add support to laravel types with common laravel types.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\LaravelPlugin\ResponseTransformers
 */
class LaravelResponseTransformer implements ResponseTransformer
{
    use ResponseDecoder;

    public function handle(ResponseInterface $response, MethodInterface $method): mixed
    {
        $returnType = $method->getReturnType();

        if (is_a($returnType, Collection::class, true)) {
            return Collection::make($this->decode($response, $method));
        } elseif (is_a($returnType, Stringable::class, true)) {
            return Str::of($response->getBody()->getContents());
        } elseif (is_a($returnType, Model::class, true)) {
            return new $returnType($this->decode($response, $method));
        }

        throw new \TypeError("The return type {$returnType} is invalid.");
    }
}