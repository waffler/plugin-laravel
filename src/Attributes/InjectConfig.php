<?php

namespace Waffler\LaravelPlugin\Attributes;

/**
 * Class Config.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\LaravelPlugin\Attributes
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class InjectConfig
{
    /**
     * This option should map to 'option_name' => 'laravel.config.path'
     * Example:
     *
     * <pre>
     * #[Config([
     *     'base_uri' => 'services.foo.base_uri', // laravel config
     *     'x' => 'literal value',          // literal value
     * ])]
     * interface FooClient {}
     * </pre>
     *
     * @param array $config
     * @param bool  $singleton
     */
    public function __construct(
        public array $config = [],
        public bool $singleton = false
    ) {
    }
}
